javascript:(function(){

  function getSelectionHtml() {
    var html = "";
    if(typeof window.getSelection != "undefined") {
      var sel = window.getSelection();
      if(sel.rangeCount) {
        var container = document.createElement("div");
        for(var i = 0, len = sel.rangeCount; i < len; ++i) {
          container.appendChild(sel.getRangeAt(i).cloneContents());
        }
        html = container.innerHTML;
      }
    } else if (typeof document.selection != "undefined") {
      if(document.selection.type == "Text") {
        html = document.selection.createRange().htmlText;
      }
    }
    return html;
  }

  function cleanseAmount(amount) {
    return Number(amount.replace(/[^0-9\.]+/g,""));
  }

  function buildSimpleUrl(name, amount) {
    var url = 'https://simple.com/a/goals/create'
            + '?name='
            + encodeURIComponent(name)
            + '&amount='
            + amount;
    return url;
  }

  function throwSelectionError() {
    alert("Make it easier to set a new Simple Goal! "
        + "Highlight the price in the page and we'll pass it on to Simple.");
  }

  function createSimpleGoal(name, amount) {
    location.href = buildSimpleUrl(name, amount);
  }

  var name = document.title;

  var selection = getSelectionHtml();
  if(!selection) throwSelectionError();

  var amount = cleanseAmount(selection);

  createSimpleGoal(name, amount);

})();
